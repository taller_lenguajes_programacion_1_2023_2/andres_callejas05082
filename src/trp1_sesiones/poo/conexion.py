
import json
import sqlite3


class Conexion:
    def __init__(self) :
        __nom_db = "db_poli.sqlite"
        self.__querys = self.obtener_json()
        self.__conx = sqlite3.connect("./src/trp1_sesiones/poo/static/db/{}".format(__nom_db))
        self.__cursor = self.__conx.cursor()
        
        
    def obtener_json(self):
        #E:/taller_programacion1/andres_callejas05082/src/trp1_sesiones/poo/static/json/querys.json
        ruta = "./src/trp1_sesiones/poo/static/json/querys.json"
        querys = {}
        with open(ruta,'r') as file_json:
            querys = json.load(file_json)
        return querys
    
    def crear_tabla(self,nom_tabla = "", nom_json = ""):
        if nom_tabla != "" and nom_json != "":
            columns = self.__querys[nom_json]
            query = self.__querys["crear_tabla"].format(nom_tabla,columns)
            self.__cursor.execute(query)
            self.__conx.commit()
            return True
        return False
    
    def insert_datos(self,nom_tabla="", nom_columnas="", valores=""):
        if nom_tabla!=""and len(nom_columnas) >0 and len(valores)>0:
            query = self.__querys["insertar_datos"]
            nom_columnas = self.__querys[nom_columnas]
            query = query.format(nom_tabla,nom_columnas,valores)
            self.__cursor.execute(query)
            self.__conx.commit()

            return True
        else:
            return False
    
    def update_datos(self,nom_tabla ="", columns_valores="",id = ""):
        if nom_tabla != "" and id != "":
            query = self.__querys["update_datos"]
            query = query.format(nom_tabla,columns_valores,id)
            self.__cursor.execute(query)
            self.__conx.commit()
            return True
        else:
            return False
    
    def delete_datos(self,nom_tabla ="", id = ""):
        if nom_tabla != "" and id != "":
            query = self.__querys["delete_datos"] 
            query = query.format(nom_tabla,id)
            self.__cursor.execute(query)
            self.__conx.commit()
            return True
        else:
            return False
            
      
 
# nom_tabla =   "personas"
# nom_columns = '"id",    "nom1",    "nom2",    "apell1",    "apell2",    "f_nac",    "tel",    "cel","email",    "descrip"'
# valores = '10,"andres","felipe","callejas","jramillo","27/09/2023",7862984,3000980998,"andres@gmail.com","ing de datos"'
# conx = Conexion()
# conx.insert_datos(nom_tabla=nom_tabla,nom_columnas=nom_columns,valores=valores)
# # print(conx)