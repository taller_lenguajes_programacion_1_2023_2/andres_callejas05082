#POO 
from estudio import Estudio
from habilidad import Habilidad
from laboral import Laboral
from excel import Excel
from conexion import Conexion
# la progracion poo tiene 4 pilares fundamentales : herencia , adstracion , polimorfismo y encapsulamiento

# clases atributos y funciones.


class Persona(Conexion):
    def __init__(self,id = 0,nom1="",nom2="",apell1="",apell2="",f_nac="",tel = 0,cel = 0,email="",descrip=""):
        
        self.__xlsx = Excel("hoja_vida.xlsx")
        self.__df_per = self.__xlsx.leer_xlsx("personas")
        self.__lista_per=self.__df_a_personas()
        self.__id = id
        self.__nom1=nom1
        self.__nom2=nom2
        self.__apell1=apell1
        self.__apell2=apell2
        self.__f_nac=f_nac
        self.__tel = tel
        self.__cel = cel
        self.__email=email
        self.__descrip=descrip
        self.__estudios = Estudio()
        self.__habilidades = Habilidad()
        self.__laboral = Laboral()
        super().__init__()
        self.__crear_Persona()
        self.__insert_Personas()
        
        

        

        
    def __crear_Persona(self):
        if self.crear_tabla(nom_tabla="personas",nom_json="nom_per"):
            print("Tabla personas creada !!!")
    
    
    def obtener_id(self,num=-1):
        if num < 0:
            return self.__lista_per["numero"]
        else:
            self.__id= self.__lista_per["numero"][num]
        return self.__id
    
    def __insert_Personas(self):
        df = self.__df_per
        nom_colunas = "colums_per"
        for index, row in df.iterrows():
            valores = '{}, "{}" , "{}" , "{}" , "{}" , "{}" , {} , {} , "{}" , "{}" , {} , {} , {}'.format(row["numero"],row["nom1 "],row["nom2"],row["apell1"],row["apell2"],row["f_nac"],row["tel"],row["cel"],row["email"],row["descrip"],row["estudios"],row["habilidades"],row["laboral"])
            if self.insert_datos(nom_tabla="personas",nom_columnas=nom_colunas,valores=valores):
                print("SI inserto en la personas : ",valores)
            else:
                print("NO INSERTO en la personas : ",valores)
            

    @property
    def _lista_per(self):
        return self.__lista_per

    @_lista_per.setter
    def _lista_per(self, value):
        self.__lista_per = value

    @property
    def _id(self):
        return self.__id

    @_id.setter
    def _id(self, id=0):
        self.__id = id


    @property
    def _nom1(self):
        return self.__nom1

    @_nom1.setter
    def _nom1(self, value):
        self.__nom1 = value

    @property
    def _nom2(self):
        return self.__nom2

    @_nom2.setter
    def _nom2(self, value):
        self.__nom2 = value

    @property
    def _apell1(self):
        return self.__apell1

    @_apell1.setter
    def _apell1(self, value):
        self.__apell1 = value

    @property
    def _apell2(self):
        return self.__apell2

    @_apell2.setter
    def _apell2(self, value):
        self.__apell2 = value

    @property
    def _f_nac(self):
        return self.__f_nac

    @_f_nac.setter
    def _f_nac(self, value):
        self.__f_nac = value

    @property
    def _tel(self):
        return self.__tel

    @_tel.setter
    def _tel(self, value):
        self.__tel = value

    @property
    def _cel(self):
        return self.__cel

    @_cel.setter
    def _cel(self, value):
        self.__cel = value

    @property
    def _email(self):
        return self.__email

    @_email.setter
    def _email(self, value):
        self.__email = value

    @property
    def _descrip(self):
        return self.__descrip

    @_descrip.setter
    def _descrip(self, value):
        self.__descrip = value

    @property
    def _estudios(self):
        return self.__estudios

    @_estudios.setter
    def _estudios(self, value):
        self.__estudios = value

    @property
    def _habilidades(self):
        return self.__habilidades

    @_habilidades.setter
    def _habilidades(self, value):
        self.__habilidades = value

    @property
    def _laboral(self):
        return self.__laboral

    @_laboral.setter
    def _laboral(self, value):
        self.__laboral = value

    def __df_a_personas(self):
        df = self.__df_per
        lista_per = df.to_dict()
        return lista_per
        # for index, row in df.iterrows():
        #     self.__id = df.numero[index]
        #     self.__nom1 = df.nom1[index]
        #     self.__nom2 = df.nom2[index]
        #     self.__apell1 = df.apell1[index]
        #     self.__apell2 = df.apell2[index]
        #     self.__f_nac = df.f_nac[index]
        #     self.__tel = df.tel[index]
        #     self.__cel = df.cel[index]
        #     self.__email = df.email[index]
        #     self.__descrip = df.descrip[index]
            

        

    
    def __str__(self):
        return f"{self.__id} {self.__nom1} {self.__estudios} {self.__habilidades} {self.__laboral}"
        


        