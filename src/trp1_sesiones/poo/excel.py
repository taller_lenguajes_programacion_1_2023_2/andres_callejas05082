import pandas as pd

class Excel:
    def __init__(self,nombre_xlsx= "") -> None:
        self.___nombre_xlsx = nombre_xlsx
        #E:\taller_programacion1\andres_callejas05082./src/trp1_sesiones/poo/static/xlsx/hoja_vida.xlsx
        self.__ruta_xlsx = "./src/trp1_sesiones/poo/static/xlsx/{}".format(self.___nombre_xlsx)
        
    
    def leer_xlsx(self, nom_hoja=""):
        if nom_hoja == "":
            df = pd.read_excel(self.__ruta_xlsx)
        else:
            df = pd.read_excel(self.__ruta_xlsx,sheet_name=nom_hoja)
        return df