# ***************  Dataframe con pandas ******************


# pandas los como estructuras bidimencionales 

datos = {
    "Nombre": ["Andres","felipe"],
    "Edad":[89],
    "Profesión":["Ingenieria"],
    "Teléfono":[97987897]
}
datos_4 = {
    "Nombre": "Andres",
    "Edad":89,
    "Profesión":"Ingenieria",
    "Teléfono":97987897
}
datos_2=["Andres",89,"Ingenieria",97987897]
#datos_3 =[datos["Nombre"],datos["Edad"],datos["Profesión"],datos["Teléfono"]]
# importar librerias
import pandas as pd

# df = pd.DataFrame(data=diccionario)
# print(df)
#E:/taller_programacion1/andres_callejas05082/src/trp1_sesiones/static/xlsx/clase_4_dataset.xlsx
df = pd.read_excel("E:/taller_programacion1/andres_callejas05082/src/trp1_sesiones/static/xlsx/clase_4_dataset.xlsx")
#columna vacia
#df["Fecha"]=""

# fila vacia
# df.loc[len(df)] = ["",0,"",0]
df.loc[len(df)] = datos_4

# df = df.append(datos_4,ignore_index=True)
df.at[2,"Profesión"] = "Contadora"
print(df.at[2,"Profesión"] )
# df.loc[7] = datos
# print(df.dtypes)

# recorrer un dataframe

for index,row in df.iterrows():
    print("la fila {} , columna {}".format(index,row))

